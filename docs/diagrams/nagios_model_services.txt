@startuml nagios_model_services.png

class Contact {
	+contact_name
}
class Command {
	+command_name
	+command_line
}
class Service {
	+service_description
}
class Servicegroup {
	+servicegroup_name
}
class Timeperiod {
	+timeperiod_name
	+[weekday]
}
class Contactgroup {
	+contactgroup_name
}
Contactgroup --> "*" Contact : members
Servicegroup --> "*" Service : members 
Service -left-> "*" Hostgroup : hostgroup_name
Service -left-> "*" Host : host_name
Contactgroup "*" <--  Service  : contact_groups
Service -> "*" Contact : contacts
Service --> "1" Command : check_command
Service --> "1" Timeperiod : check_period
Service --> "1" Timeperiod : notification_period
Contact --> "*" Command : service_notification_commands
Contact --> "1" Timeperiod : service_notification_period
Timeperiod --> "*" Timeperiod : use


@enduml